FROM gradle:5.6.0

#COPY --chown=gradle:gradle . /app
#WORKDIR /app
RUN gradle build


COPY build/libs/*.jar home/spring/application.jar

EXPOSE 8080
#WORKDIR /app

ENTRYPOINT ["java", "-jar", "/home/spring/application.jar"]

#CMD java -jar build/libs/demo-0.0.1-SNAPSHOT.jar


#FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]
