package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class DemoController {

    private AtomicLong counter = new AtomicLong();

    @Autowired
    Operaciones operaciones;

    @GetMapping("/calcular")
    public String calcular(){
        StringBuilder stb = new StringBuilder();

        stb.append(operaciones.suma()+"\n");
        stb.append(operaciones.restar()+"\n");
        stb.append(operaciones.multiplicar()+"\n");
        stb.append(operaciones.dividir()+"\n");
//        Response response = new Response();
//        response.addHeader("suma",operaciones.suma());
//        response.addHeader("resta",operaciones.restar());
//        response.addHeader("multiplicacion",operaciones.multiplicar());
//        response.addHeader("division",operaciones.dividir());

//        return stb.toString();
        return stb.toString();
    }


    @GetMapping("/getPerson")
    public Person getPerson(@RequestHeader(value = "name", defaultValue = "World")String name  ){
        return  new Person(counter.incrementAndGet(), name);
    }


}
