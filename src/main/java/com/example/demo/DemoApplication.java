package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	@Autowired
	DemoController controller;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

		System.out.println("It´s working");

	}




}
