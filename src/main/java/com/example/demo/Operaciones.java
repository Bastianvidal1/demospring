package com.example.demo;


import org.springframework.stereotype.Service;

@Service
public class Operaciones {


    public String suma(){
        String statement = "2 + 2 =  4";
        System.out.println(statement);
        return statement;
    }

    public String restar(){
        String statement = "2 - 2 = 0";
        System.out.println(statement);
        return statement;
    }

    public String multiplicar(){
        String statement = "2 * 2 =  4";
        System.out.println(statement);
        return statement;
    }

    public String dividir(){
        String statement = "2 / 2 =  1";
        System.out.println(statement);
        return statement;
    }



}
